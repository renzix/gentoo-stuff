# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="a bitmap font"
HOMEPAGE="https://github.com/turquoise-hexagon/cherry"

LICENSE="MIT"
SLOT="0"

inherit font font-ebdftopcf

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
else
	SRC_URI="
		${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz
	"
	KEYWORDS="~*"
fi

IUSE=""

DEPEND="x11-apps/bdftopcf"
RDEPEND="${DEPEND}"
