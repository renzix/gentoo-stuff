# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit font

DESCRIPTION="A programming font with ligatures"
HOMEPAGE="https://github.com/tonsky/FiraCode"
SRC_URI="
	"https://github.com/tonsky/FiraCode/raw/${PV}/distr/ttf/FiraCode-Bold.ttf"
	"https://github.com/tonsky/FiraCode/raw/${PV}/distr/ttf/FiraCode-Light.ttf"
	"https://github.com/tonsky/FiraCode/raw/${PV}/distr/ttf/FiraCode-Medium.ttf"
	"https://github.com/tonsky/FiraCode/raw/${PV}/distr/ttf/FiraCode-Regular.ttf"
	"https://github.com/tonsky/FiraCode/raw/${PV}/distr/ttf/FiraCode-Retina.ttf"
"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}"

FONT_S="${S}"
FONT_SUFFIX="ttf"

src_unpack() {
	cp "${DISTDIR}"/*.ttf "${WORKDIR}"
}

src_install() {
	insinto /usr/share/fonts/${PN}

	cd "${WORKDIR}"
	doins *.ttf

	font_xfont_config
	font_fontconfig
}
