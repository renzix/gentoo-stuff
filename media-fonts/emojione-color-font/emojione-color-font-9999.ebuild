# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3
EGIT_REPO_URI="https://github.com/eosrei/${PN}"
REQUIRED_USE="!binary"
inherit font-r1

DESCRIPTION="A color emoji SVGinOT font using EmojiOne Unicode 9.0 emoji"
HOMEPAGE="https://github.com/eosrei/emojione-color-font"

LICENSE="CC-BY-4.0 MIT"
SLOT="0"
IUSE="+binary"

DEPEND="
	!binary? (
		media-gfx/inkscape
		virtual/imagemagick-tools[png]
		media-gfx/potrace
		dev-util/svgo
		dev-python/scfbuild
		dev-python/fonttools
		media-gfx/fontforge
	)
"

pkg_setup() {
	local _fc="fontconfig/56-${PN%-*}.conf"
	if use binary; then
		S="${WORKDIR}/${MY_P}"
		FONT_CONF="${S}/${_fc}"
	else
		FONT_S=( build )
		FONT_CONF="${S}/linux/${_fc}"
		DOCS="*-demo.html"
	fi
	font-r1_pkg_setup
}

src_prepare() {
	default
	use binary && return

	sed -e '/all:/ s:$(OSX_FONT)::' -i "${S}"/Makefile
	addpredict /dev/dri
}

src_compile() {
	use binary && return

	emake \
		SCFBUILD="${EROOT}usr/bin/scfbuild"
}
