# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit font

DESCRIPTION="Iconic font aggregator, collection, and patcher"
HOMEPAGE="https://nerdfonts.com/"
SRC_URI="https://github.com/ryanoasis/nerd-fonts/archive/v2.0.0.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~*"

S="${WORKDIR}"

FONT_S="${S}"
FONT_SUFFIX="ttf"

src_install() {
	insinto /usr/share/fonts/${PN}

	cd "${WORKDIR}/${PN}-${PV}/patched-fonts"

	find . -type f -name "*.ttf" -or -name "*.TTF" -or -name "*.otf" | while read -r line
	do
		[ -z "$line" ] && continue
		doins "$line"
	done

	font_xfont_config
	font_fontconfig
}
