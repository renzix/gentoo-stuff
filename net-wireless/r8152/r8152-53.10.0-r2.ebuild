# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit eutils linux-info linux-mod

DESCRIPTION="r8152 driver for USB C ethernet adaptor"
HOMEPAGE="http://www.realtek.com/downloads/downloadsView.aspx?Langid=1&PNid=56&PFid=56&Level=5&Conn=4&DownTypeID=3&GetDown=false"
SRC_URI="https://az695102.vo.msecnd.net/rtdrivers/cn/nic/0010-r8152.53-2.10.0.tar.bz2 -> ${P}.tar.bz2"

RESTRICT="mirror bindist" # bindist restricted due to patents (see: PATENTS in r8152.c)

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-* ~amd64 ~x86"
IUSE="+udev"

DEPEND="virtual/linux-sources"
RDEPEND="
	udev? (
		virtual/udev
	)
"

S="${WORKDIR}/r8152-2.10.0"

MODULE_NAMES="r8152(net/usb)"
MODULESD_WL_ALIASES=("r8152")

pkg_setup() {
	linux-mod_pkg_setup

	BUILD_PARAMS="-C ${KV_DIR} M=${S}"
	BUILD_TARGETS="r8152.ko"
}

src_install() {
	linux-mod_src_install

	if use udev
	then
		# shitty Makefile is shitty
		sed -i "s|/etc/udev/rules.d/|${ED}etc/udev/rules.d/|g" Makefile
		install -dm0755 "${ED}etc/udev/rules.d/"
		emake install_rules
	fi

	dodoc "ReadMe.txt"
}
