#!/bin/sh

errexit() {
  if [ -n "$1" ] ; then printf "%s\n" "$1"; fi
  exitcode="${2:-1}"

  exit "$exitcode"
}

[ -f "$(basename "$0")" ] && {
  cd ../ || errexit "Unable to change directory to $PWD/.."
}

bashlate="$PWD/.scripts/bashlate"

[ ! -f "$bashlate" ] && errexit "$bashlate not found"

rm -f .gitlab-ci.yml

includes="$(find . -name .gitlab-ci.yml | sort -h | sed -e "s|^./|/|g" -e "s|^|  - '|g" -e "s|\$|'|g")"

[ -z "$includes" ] && errexit "\$includes is empty: $includes"

# export includes for our bash templating script to use...
export includes

"$bashlate" .gitlab-ci.yml.tmpl > .gitlab-ci.yml
