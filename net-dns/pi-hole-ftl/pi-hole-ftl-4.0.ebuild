# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="The Pi-hole FTL engine"
HOMEPAGE="https://github.com/pi-hole/FTL"
SRC_URI="
	${HOMEPAGE}/archive/v${PV}.tar.gz -> ${PN}-${PV}.tar.gz
	man? ( https://raw.githubusercontent.com/pi-hole/pi-hole/v${PV}/manpages/pihole-FTL.8 -> ${PN}-${PV}_pihole-FTL.8 )
	man? ( https://raw.githubusercontent.com/pi-hole/pi-hole/v${PV}/manpages/pihole-FTL.conf.5 -> ${PN}-${PV}_pihole-FTL.conf.5 )
"

inherit eutils readme.gentoo-r1 systemd user

RESTRICT="mirror"

LICENSE="EUPL-1.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+caps +man systemd"

DEPEND="
	caps? ( sys-libs/libcap )
	dev-db/sqlite
"
RDEPEND="${DEPEND}"

S="${WORKDIR}/FTL-${PV}"

# shoutout to the AUR maintainer: https://aur.archlinux.org/packages/pi-hole-ftl/
src_prepare() {
	local _ssc="$T/sedcontrol"

	! use elibc_glibc && {
		epatch "${FILESDIR}/nonglibc_dont_obtain_backtrace.patch" || die
	}

	epatch "${FILESDIR}/fix_dnsmasq_include-${PV}.patch" || die

	# To do add static USE flag
	epatch "${FILESDIR}/patch_Makefile-${PV}.patch"

	# git descriptions setup
	sed -i "s|^GIT_BRANCH := .*$|GIT_BRANCH := master|w $_ssc" Makefile
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: git descriptions setup 1" && die; fi
	sed -i "s|^GIT_VERSION := .*$|GIT_VERSION := v$PV|w $_ssc" Makefile
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: git descriptions setup 2" && die; fi
	sed -i "s|^GIT_DATE := .*$|GIT_DATE := $(date -I)|w $_ssc" Makefile
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: git descriptions setup 3" && die; fi
	sed -i "s|^GIT_TAG := .*$|GIT_TAG := v$PV|w $_ssc" Makefile
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: git descriptions setup 4" && die; fi

	# setting up logs paths
	#sed -i "s|/var/log/pihole-FTL.log|/run/log/pihole-ftl/pihole-FTL.log|w $_ssc" memory.c
	#if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: setting up logs paths 1" && die; fi
	sed -i "s|/var/run/pihole-FTL|/run/pihole-ftl/pihole-FTL|w $_ssc" memory.c
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: setting up logs paths 2" && die; fi
	sed -i "s|/var/run/pihole/|/run/pihole-ftl/|w $_ssc" memory.c
	if [ -s $_ssc ] ; then rm $_ssc ; else echo "   ==> Sed error: setting up logs paths 4" && die; fi

	default
}

src_install() {
	dobin pihole-FTL

	use systemd && {
		systemd_dounit "${FILESDIR}/${PN}.service"
		systemd_newtmpfilesd "${FILESDIR}/${PN}.tmpfile" "${PN}.conf"
	}

	install -dm755 "${ED}"/etc/pihole
	install -Dm644 "${FILESDIR}/${PN}.conf" "${ED}"/etc/pihole/pihole-FTL.conf

	newinitd "${FILESDIR}/pihole-FTL.initd.10" pihole-FTL

	use man && {
		for f in "${DISTDIR}"/{${PN}-${PV}_pihole-FTL.8,${PN}-${PV}_pihole-FTL.conf.5}
		do
			_f="${f/${PN}-${PV}_/}"
			_f="${_f##*/}"
			newman "$f" "$_f"
		done
	}

	DOC_CONTENTS="
Possible configurations in \e[1;31m/etc/pihole/pihole-FTL.conf\e[0m file
Please read the instructions on the project page: \e[1;36mhttps://github.com/pi-hole/FTL#ftls-config-file\e[0m
"

	readme.gentoo_create_doc
}

pkg_postinst() {
	readme.gentoo_print_elog
	enewuser pihole || die
	touch /var/log/pihole-FTL.log
	chown pihole:pihole /var/log/pihole-FTL.log
	chown pihole:pihole /etc/pihole
	mkdir -p /run/pihole-ftl
	chown pihole:pihole /run/pihole-ftl
	use caps && /sbin/setcap cap_net_bind_service,cap_net_raw,cap_net_admin+ei /usr/bin/pihole-FTL
}

pkg_postrm() {
	id pihole > /dev/null 2>&1 && userdel -f pihole
}
