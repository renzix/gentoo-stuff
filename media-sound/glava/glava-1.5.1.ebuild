# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python3_{4,5,6} pypy{,3} )

DESCRIPTION="cli and curses mixer for pulseaudio"
HOMEPAGE="https://github.com/wacossusca34/glava"
SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="pulseaudio opengl"

# The build fails fetching some xml definitions but I'm too lazy to fix this :P
# To Do:
#		* Don't be lazy
RESTRICT="network-sandbox"

RDEPEND="
		media-sound/pulseaudio
		media-libs/glfw
		dev-python/glad
"
DEPEND="${RDEPEND}
"

src_compile() {
	make || die
}

src_install() {
	unset XDG_CONFIG_DIRS
	make DESTDIR="${ED}" install
}

