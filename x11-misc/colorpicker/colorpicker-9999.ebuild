# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

DESCRIPTION="Tool for X11 that writes the RGB color value at the cursors position to stdout"
HOMEPAGE="https://github.com/Ancurio/colorpicker"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="X"

RDEPEND="
	x11-libs/gtk+:2
"
DEPEND="${RDEPEND}
"

src_compile() {
	make
}

src_install() {
	dobin colorpicker
}

#S=${WORKDIR}/${P}-license
