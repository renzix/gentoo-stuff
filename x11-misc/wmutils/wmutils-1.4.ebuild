# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A set of tools for X windows manipulation."
HOMEPAGE=" https://github.com/wmutils/core"
SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND=""
DEPEND="${RDEPEND}
"

S="${WORKDIR}/core-${PV}"

src_compile() {
	make DESTDIR="${ED}"
}

src_install() {
	for bin in $(find . -maxdepth 1 -type f -executable)
	do
		dobin "$bin"
	done

	doman man/*.*

	dodoc README.md
}
