# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

DESCRIPTION="A set of tools for X windows manipulation."
HOMEPAGE=" https://github.com/wmutils/core"
EGIT_REPO_URI=" https://github.com/wmutils/core.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND=""
DEPEND="${RDEPEND}
"

#S=${WORKDIR}

src_compile() {
	make DESTDIR="${ED}"
}

src_install() {
	for bin in $(find . -maxdepth 1 -type f -executable)
	do
		dobin "$bin"
	done

	doman man/*.*

	dodoc README.md
}
