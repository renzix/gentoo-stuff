# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

DESCRIPTION="a featherweight, lemon-scented, bar based on xcb"
HOMEPAGE="https://github.com/LemonBoy/bar"

EGIT_REPO_URI="${HOMEPAGE}.git"
[[ "$USE" == *"xft"* && "$USE" != *"-xft"* || "$USE" == *"-xft"*"xft"* && "$USE" != *"-xft"*"-xft"* ]] && EGIT_REPO_URI="https://github.com/krypt-n/bar.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="+xft"

CDEPEND="
	x11-libs/libxcb
	xft? ( x11-libs/libXft )
"
DEPEND="
	dev-lang/perl
	${CDEPEND}
"
RDEPEND="
	${CDEPEND}
"

src_prepare() {
	default
}

pkg_postinst() {
	use xft && {
		elog "The xft useflag is enabled"
		elog "Please direct any upstream bug reports to the following fork:"
		elog "https://github.com/krypt-n/bar"
	}
}
