# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Pi-hole is an advertising-aware DNS/Web server"
HOMEPAGE="https://pi-hole.net/"

_wwwpkgver=3.3

SRC_URI="
	https://github.com/pi-hole/pi-hole/archive/v${PV}.tar.gz -> ${PN}-${PV}.tar.gz
	https://github.com/pi-hole/AdminLTE/archive/v${_wwwpkgver}.tar.gz -> ${PN}_AdminLTE-${_wwwpkgver}.tar.gz
"

inherit eutils systemd user

RESTRICT="mirror"

LICENSE="EUPL-1.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug +ftl lighttpd +logrotate nginx systemd"

# To Do:
#	* Test without logrotate and ftl
REQUIRED_USE="logrotate ftl"

_php_deps="pdo,sqlite,sockets"

DEPEND="
	ftl? ( net-dns/pi-hole-ftl )
	lighttpd? (
		www-servers/lighttpd
		dev-lang/php:=[cgi,$_php_deps]
	)
	nginx? (
		www-servers/nginx:=
		dev-lang/php:=[fpm,$_php_deps]
	)
	!lighttpd? (
		!nginx? (
			dev-lang/php:=[$_php_deps]
		)
	)
	logrotate? ( app-admin/logrotate )
	!systemd? ( sys-apps/init-system-helpers )
	systemd? ( sys-apps/systemd )
	app-admin/sudo
	net-dns/dnsmasq
	dev-lang/perl
	sys-devel/bc
	sys-apps/iproute2
	sys-apps/net-tools
"
RDEPEND="${DEPEND}"

S="${WORKDIR}/pi-hole-${PV}"

PATCHES=(
	"${FILESDIR}/disable_automatic_updates.patch"
	"${FILESDIR}/install_into_srv_http.patch"
	"${FILESDIR}/setup_gravity.patch"
	"${FILESDIR}/setup_webpage.patch"
	"${FILESDIR}/update_logrotate_paths.patch"
)

_adminLTEDIR="${WORKDIR}/AdminLTE-$_wwwpkgver"

src_prepare() {
	if use systemd; then
		PATCHES+=("${FILESDIR}/systemd_use_systemctl_for_service_management.patch")
		PATCHES+=("${FILESDIR}/killall-use-systemctl-kill-for-reload_${PV}.patch")
	else
		PATCHES+=("${FILESDIR}/killall-use-full-path-for-reload_${PV}.patch")
	fi

	local _ssc="${T}/sedcontrol"

	cd "$_adminLTEDIR" || die
	! use debug && {
		epatch "${FILESDIR}"/adminLTE_disable_debug_logs.patch || die;
	}
	epatch "${FILESDIR}"/adminLTE_update_checker.patch || die
	cd -

	epatch "${FILESDIR}"/version.patch

	sed -i "s/{{corever}}/${PV}/" advanced/Scripts/version.sh
	sed -i "s/{{webver}}/$_wwwpkgver/" advanced/Scripts/version.sh

	sed -i "s|/var/run/pihole-FTL.port|/run/pihole-ftl/pihole-FTL.port|w $_ssc" "$_adminLTEDIR"/scripts/pi-hole/php/FTL.php

	# sudo pihole full path (php-fpm compatibility)
	sed -i "s|sudo pihole|/usr/bin/sudo /usr/bin/pihole|w $_ssc" "$_adminLTEDIR"/api.php
	if [ -s $_ssc ] ; then rm $_ssc ; else die "   ==> Sed error: sudo pihole full path (php-fpm compatibility) 1"; fi
	sed -i "s|sudo pihole|/usr/bin/sudo /usr/bin/pihole|w $_ssc" "$_adminLTEDIR"/scripts/pi-hole/php/header.php
	if [ -s $_ssc ] ; then rm $_ssc ; else die "   ==> Sed error: sudo pihole full path (php-fpm compatibility) 2"; fi
	sed -i "s|sudo pihole|/usr/bin/sudo /usr/bin/pihole|w $_ssc" "$_adminLTEDIR"/scripts/pi-hole/php/queryads.php
	if [ -s $_ssc ] ; then rm $_ssc ; else die "   ==> Sed error: sudo pihole full path (php-fpm compatibility) 3"; fi
	sed -i "s|sudo pihole|/usr/bin/sudo /usr/bin/pihole|w $_ssc" "$_adminLTEDIR"/scripts/pi-hole/php/savesettings.php
	if [ -s $_ssc ] ; then rm $_ssc ; else die "   ==> Sed error: sudo pihole full path (php-fpm compatibility) 4"; fi
	sed -i "s|sudo pihole|/usr/bin/sudo /usr/bin/pihole|w $_ssc" "$_adminLTEDIR"/scripts/pi-hole/php/sub.php
	if [ -s $_ssc ] ; then rm $_ssc ; else die "   ==> Sed error: sudo pihole full path (php-fpm compatibility) 5"; fi
	sed -i "s|sudo pihole|/usr/bin/sudo /usr/bin/pihole|w $_ssc" "$_adminLTEDIR"/scripts/pi-hole/php/add.php
	if [ -s $_ssc ] ; then rm $_ssc ; else die "   ==> Sed error: sudo pihole full path (php-fpm compatibility) 6"; fi
	sed -i "s|sudo pihole|/usr/bin/sudo /usr/bin/pihole|w $_ssc" "$_adminLTEDIR"/scripts/pi-hole/php/gravity.sh.php
	if [ -s $_ssc ] ; then rm $_ssc ; else die "   ==> Sed error: sudo pihole full path (php-fpm compatibility) 7"; fi
	sed -i "s|sudo pihole|/usr/bin/sudo /usr/bin/pihole|w $_ssc" "$_adminLTEDIR"/scripts/pi-hole/php/loginpage.php
	if [ -s $_ssc ] ; then rm $_ssc ; else die "   ==> Sed error: sudo pihole full path (php-fpm compatibility) 8" ; fi

	# pi-hole sudoers file is now populated by install script
	echo "nobody ALL=NOPASSWD: /usr/bin/pihole" >> advanced/pihole.sudo

	default
}

src_install() {
	dobin pihole

	insinto /opt/pihole

	for f in \
		'gravity.sh' \
		advanced/Scripts/{version.sh,piholeLogFlush.sh,chronometer.sh,list.sh,webpage.sh,COL_TABLE} \
		"${FILESDIR}/piholeDebug.sh" \
		"${FILESDIR}/mimic_setupVars.conf.sh"
	do
		doins "$f"
		chmod 755 "${D}opt/pihole/${f##*/}"
	done

	insinto /etc/sudoers.d
	newins advanced/pihole.sudo pihole
	chmod 440 "${D}/etc/sudoers.d/${_##*/}"

	use systemd && {
		for f in 'pi-hole-gravity.service' 'pi-hole-gravity.timer' 'pi-hole-logtruncate.service' 'pi-hole-logtruncate.timer'
		do
			systemd_dounit "${FILESDIR}/$f"
		done
		# is this needed?
		#install -dm755 "$pkgdir/usr/lib/systemd/system/multi-user.target.wants"
		#ln -s ../$_pkgname-gravity.timer "$pkgdir/usr/lib/systemd/system/multi-user.target.wants/$_pkgname-gravity.timer"
		#ln -s ../$_pkgname-logtruncate.timer "$pkgdir/usr/lib/systemd/system/multi-user.target.wants/$_pkgname-logtruncate.timer"
	}

	install -dm755 "$D"/etc/pihole
	install -dm755 "$D"/usr/share/pihole/configs
	install -Dm644 adlists.default "$D"/etc/pihole/adlists.default
	install -Dm644 advanced/logrotate "$D"/etc/pihole/logrotate
	install -Dm644 /dev/null "$D"/etc/pihole/whitelist.txt
	install -Dm644 /dev/null "$D"/etc/pihole/blacklist.txt

	install -Dm644 "${FILESDIR}"/dnsmasq.main "$D"/usr/share/pihole/configs/dnsmasq.example.conf
	install -Dm644 "${FILESDIR}"/dnsmasq.include "$D"/etc/dnsmasq.d/01-pihole.conf
	install -Dm644 "${FILESDIR}"/lighttpd.pi-hole.conf "$D"/usr/share/pihole/configs/lighttpd.example.conf
	install -Dm644 "${FILESDIR}"/nginx.pi-hole.conf "$D"/usr/share/pihole/configs/nginx.example.conf

	install -dm755 "$D"/srv/http/pihole/admin
	install -Dm644 advanced/index.php "$D"/srv/http/pihole/pihole/index.php
	#  install -Dm644 advanced/index.js "$D"/srv/http/pihole/pihole/index.js
	install -Dm644 advanced/blockingpage.css "$D"/srv/http/pihole/pihole/blockingpage.css

	cp -dpr --no-preserve=ownership "$_adminLTEDIR"/* "$D"/srv/http/pihole/admin/

	#install -dm755 "$pkgdir"/usr/share/licenses/pihole
	#install -Dm644 ${pkgname%-*}-$pkgver/LICENSE "$pkgdir"/usr/share/licenses/pihole/Pi-hole
	#install -Dm644 $_wwwpkgname-$_wwwpkgver/LICENSE "$pkgdir"/usr/share/licenses/pihole/AdminLTE
	#install -Dm644 $_wwwpkgname-$_wwwpkgver/style/vendor/SourceSansPro/OFL.txt \
	#	"$pkgdir"/usr/share/licenses/pihole/SourceSansPro

	rm -f "$D"/srv/http/pihole/admin/*.md
	rm -f "$D"/srv/http/pihole/admin/LICENSE
	rm -f "$D"/srv/http/pihole/admin/style/vendor/LICENSE
	rm -f "$D"/srv/http/pihole/admin/scripts/vendor/LICENSE
	rm -f "$D"/srv/http/pihole/admin/style/vendor/SourceSansPro/OFL.txt
}

pkg_postinst() {
	! use lighttpd && ! use nginx && {
		ewarn "You have chosen not to use one of the known working web servers"
		ewarn "In theory this is fine but has not been tested! You are on your own!"
		ewarn "Depending on your web server you may need to re-build dev-lang/php with the fpm or cgi usevar"
	}
	enewuser pihole || die
	[ ! -e /var/log/pihole.log ] && touch /var/log/pihole.log
	chown -R pihole:pihole /var/log/pihole.log
	elog "The default sudoers config at /etc/sudoers.d/pihole assumes php-fpm is running as the nobody user"
	elog "Please edit this accordingly if this is _not_ the case"
	[ -e /etc/pihole/setupVars.conf ] && [ ! -d /etc/pihole/setupVars.conf ] && return "$?"
	/opt/pihole/mimic_setupVars.conf.sh
}
