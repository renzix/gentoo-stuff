# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Standalone MPD Web GUI written in C"
HOMEPAGE="https://github.com/notandy/ympd"

LICENSE="GPL-2"
SLOT="0"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
else
	SRC_URI="
		${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz
	"
	KEYWORDS="~amd64 ~x86"
fi

IUSE="ipv6 libressl mpd-host-change +ssl"

RDEPEND="
	media-libs/libmpdclient
	ssl? (
		libressl? (
			dev-libs/libressl:*
		)
		!libressl? (
			dev-libs/openssl:*
		)
	)
"
DEPEND="
dev-util/cmake
${RDEPEND}
"
BDEPEND=""

src_compile() {
	configure_args=()
	use ipv6 && configure_args+=("-DWITH_IPV6=ON") || configure_args+=("-DWITH_IPV6=OFF")
	use ssl && configure_args+=("-DWITH_SSL=ON") || configure_args+=("-DWITH_SSL=OFF")
	! use mpd-host-change && configure_args+=("-DWITH_MPD_HOST_CHANGE=OFF") || configure_args+=("-DWITH_MPD_HOST_CHANGE=ON")
	mkdir build
	cd build
	cmake .. -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_INSTALL_PREFIX=/usr ${configure_args[@]}
}

src_install() {
	cd build
	make DESTDIR="${D}" install
	cd -
	dodoc "README.md"
	insinto /usr/share/${PN}
	doins -r htdocs
	newinitd "${FILESDIR}/${PN}.init" "${PN}"
}
