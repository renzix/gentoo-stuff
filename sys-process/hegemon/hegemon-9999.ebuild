# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit cargo git-r3

DESCRIPTION="A modular system monitor written in Rust"
HOMEPAGE="https://github.com/p-e-w/hegemon"
EGIT_REPO_URI="${HOMEPAGE}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="fetch-crates"

# needed for cargo to work :(
RESTRICT="network-sandbox"

DEPEND="
	$CDEPEND
	>=dev-lang/rust-1.15.0
"
RDEPEND="${CDEPEND}"

src_test() {
	cargo test
}
