# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="RFCs from rfc-editor.org"
HOMEPAGE="https://www.archlinux.org/packages/community/any/rfc/"
SRC_URI="
	https://sources.archlinux.org/other/community/rfc/rfc-index-${PV}.txt
	https://sources.archlinux.org/other/community/rfc/RFC-all-${PV}.tar.gz
	https://git.archlinux.org/svntogit/community.git/plain/trunk/license?h=packages/rfc -> ${PN}-${PV}_license
"

# if you change the license here remember to change the link in licenses/rfc
LICENSE="rfc"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+doc"

RESTRICT="mirror"

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_install() {
	dodoc rfc-index.txt

	tar xf rfc1305.tar
	rm -f rfc1305.tar
	for n in ntpv3*; do
		mv "$n" "rfc1305-$n"
	done

	for fmt in txt ps pdf; do
		docinto "${fmt}"
		dodoc *.${fmt}
	done

	docinto "/"
	cp "${DISTDIR}/${PN}-${PV}_license" "license"
	dodoc "license"

	# don't version the docs
	mv "${ED}/usr/share/doc/rfc-${PV}" "${ED}/usr/share/doc/rfc"
}
