# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3 readme.gentoo-r1

DESCRIPTION="DuckDuckGo from the terminal"
HOMEPAGE="https://github.com/sunaku/dasht"
EGIT_REPO_URI="https://github.com/sunaku/dasht"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

IUSE="+zsh-completion"

RDEPEND="
	dev-db/sqlite
"

DEPEND="${RDEPEND}
"

DOC_CONTENTS="
The following optional packages can be installed to provide extra functionality:
\n
* net-misc/wget to download docsets from Dash\n
* www-client/w3m to display dasht(1) search results\n
* net-misc/socat for dasht-server(1) search engine\n
* sys-apps/gawk for dasht-server(1) search engine\n
"

src_install() {
	dobin bin/*
	doman man/man1/*

	if use zsh-completion ; then
		insinto /usr/share/zsh/site-functions
		doins etc/zsh/completions/*
	fi

	dodoc README.md VERSION.md

	readme.gentoo_create_doc
}

pkg_postinst() {
	readme.gentoo_print_elog
}
