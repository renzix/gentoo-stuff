# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit cmake-utils git-r3 gnome2-utils xdg-utils

DESCRIPTION="Offline documentation browser inspired by Dash"
HOMEPAGE="https://zealdocs.org/"

EGIT_REPO_URI="https://github.com/zealdocs/${PN}.git"
EGIT_BRANCH="master"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""

DEPEND="
	app-arch/libarchive
	dev-qt/qtconcurrent:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5
	dev-qt/qtsql:5[sqlite]
	dev-qt/qtwebkit:5
	dev-qt/qtwidgets:5
	dev-qt/qtx11extras:5
	kde-frameworks/extra-cmake-modules:5
	>=x11-libs/xcb-util-keysyms-0.3.9
"

RDEPEND="
	${DEPEND}
	x11-themes/hicolor-icon-theme
"

src_prepare() {
	# AUTOUIC is broken
	sed -i 's|set(CMAKE_AUTOUIC ON)|set(CMAKE_AUTOUIC OFF)|g' src/CMakeLists.txt
	find "$PWD" -name "*.ui" -execdir sh -c 'uic -o ui_`basename {} .ui`.h {}' \;
	default
}

pkg_preinst() {
	gnome2_icon_savelist
}

pkg_postinst() {
	gnome2_icon_cache_update
	xdg_desktop_database_update
}

pkg_postrm() {
	gnome2_icon_cache_update
	xdg_desktop_database_update
}
