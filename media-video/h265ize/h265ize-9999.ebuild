# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

SRC_URI="
	test? ( https://download.blender.org/durian/trailer/Sintel_Trailer1.480p.DivX_Plus_HD.mkv -> ${P}_sintel-test.mkv )
"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/FallingSnow/${PN}.git"
else
	SRC_URI="
		$SRC_URI
		mirror://githubcl/FallingSnow/${PN}/tar.gz/v${PV} -> ${P}.tar.gz
	"
	RESTRICT="primaryuri"
	KEYWORDS="~amd64 ~x86"
fi

inherit multilib

RESTRICT="${RESTRICT} mirror network-sandbox"

DESCRIPTION="A node utility utilizing ffmpeg to encode videos with the hevc codec."
HOMEPAGE="https://github.com/FallingSnow/$PN"

LICENSE="MIT"
SLOT="0"
IUSE="+upconvert vanilla test"

DEPEND="
	media-video/ffmpeg[x265]
	net-libs/nodejs[npm]
	app-arch/tar
	upconvert? ( media-video/mkvtoolnix media-video/vobsub2srt )
"
RDEPEND="${DEPEND}"
DOCS=( README.md settings.sample.json )

src_prepare() {
	sed -i 's|"mode": "development",$|"mode": "production",|g' package.json

	default
}

src_compile() { :; }

src_test() {
	einfo "Skipping the following tests that are known to fail:"
	einfo "Video
		* it should stop
		* it should encode a video
	"
	patch -p1 -i "${FILESDIR}/skip_failing_tests.patch"
	mkdir -p test
	cp "$DISTDIR/${P}_sintel-test.mkv" test/sintel-test.mkv
	npm install || die
	npm test || die
	rm -rf node_modules
}

src_install() {
	cd ..
	tar -czf "$PN"-"$PV".tar.gz "$PN"-"$PV"
	cd $OLDPWD
	npm install --production --cache ../cache --user root -g --prefix "${D}/usr" ../*.tar.gz
	npm outdated

	einstalldocs

	dodoc "${D}/usr/$(get_libdir)/node_modules/${PN}/LICENSE"

	if ! use vanilla
	then
		# fix up the version number
		[[ -z ${PV%%*9999} ]] && {
			sed -i "s|\"version\": .*|\"version\": \"${EGIT_BRANCH:-master}#$(git rev-parse --short HEAD)\"|g" "${D}usr/$(get_libdir)/node_modules/${PN}/package.json"
		}
		rm -rf "${D}/usr/$(get_libdir)/node_modules/${PN}"/.git*
		find "${D}/usr/$(get_libdir)/node_modules/${PN}" -type f -name "LICENSE*" -or -name "LICENCE*" -delete

		local find_exp="-or -name"
		local find_name=()
		for match in "AUTHORS*" "CHANGELOG*" "CONTRIBUT*" "README*" \
			".travis.yml" ".eslint*" ".wercker.yml" ".npmignore" \
			"*.md" "*.markdown" "*.bat" "*.cmd"; do
			find_name+=( ${find_exp} "${match}" )
		done

		# Remove various development and/or inappropriate files and
		# useless docs of dependend packages.
		find "${D}/usr/$(get_libdir)/node_modules/${PN}" \
			\( -type d -name examples \) -or \( -type f \( \
				-iname "LICEN?E*" \
				"${find_name[@]}" \
			\) \) -exec rm -rf "{}" \;
	fi
}
