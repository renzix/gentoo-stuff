# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

DESCRIPTION="An improved dynamic tiling window manager"
HOMEPAGE="https://github.com/Sweets/custard"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="doc examples"

CDEPEND="
	x11-libs/libxcb[xkb]
	x11-libs/xcb-util
	x11-libs/xcb-util-wm
	dev-libs/libconfig
"
DEPEND="${CDEPEND}"
RDEPEND="${CDEPEND}"

src_prepare() {
	sed -i 's|/usr/local|/usr|g' Makefile

	default
}

src_compile() {
	emake
}

src_install() {
	emake DESTDIR="${D}" install
	einstalldocs
	if use examples
	then
		dodoc -r examples
	fi
	insinto /usr/share/xsessions
	doins contrib/custard.desktop
}
