# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Stacking window manager that cooks windows with orders from the Waitron"
HOMEPAGE="https://github.com/tudurom/windowchef"
EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="ISC"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	x11-libs/libxcb:=
	x11-libs/xcb-util-keysyms:=
	x11-libs/xcb-util-wm:=
	x11-base/xorg-proto:=
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	emake DESTDIR="${D}" PREFIX="/usr" install
	mv "${D}/usr/share/doc/${PN}" "${D}/usr/share/doc/${P}"
}
