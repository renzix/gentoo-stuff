# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A beautiful, fast and maintained fork of Nylas Mail"
HOMEPAGE="https://getmailspring.com/"
SRC_URI="https://github.com/Foundry376/Mailspring/releases/download/${PV}/mailspring-${PV}-amd64.deb"

RESTRICT="mirror"

LICENSE="
	GPL-3
	no-source-code
"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="
	${DEPEND}
	gnome-base/libgnome-keyring
	gnome-base/gnome-keyring
	x11-libs/libXScrnSaver
	sys-devel/libtool
	net-dns/c-ares
	dev-cpp/ctemplate
	app-text/tidy-html5
	x11-libs/libxkbfile
	app-crypt/libsecret
	gnome-base/gconf
	x11-libs/gtk+:2
	dev-libs/nss
"

inherit unpacker

S="${WORKDIR}"

src_install() {
	mv usr "${ED}"
}
