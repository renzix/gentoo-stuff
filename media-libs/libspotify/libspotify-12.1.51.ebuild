# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="C API for Spotify"
HOMEPAGE="https://developer.spotify.com/technologies/libspotify"
SRC_URI="
	amd64? ( https://mopidy.github.io/libspotify-archive/${P}-Linux-x86_64-release.tar.gz )
	x86? ( https://mopidy.github.io/libspotify-archive/${P}-Linux-i686-release.tar.gz )
"

LICENSE="libspotify"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${P}-Linux-x86_64-release"

src_prepare() {
	# Don't do stupid things from a Makefile
	elog "Patching Makefile..."
	sed -i 's/ldconfig//' Makefile
	default
}

src_compile() {
	:
}

src_install() {
	emake prefix="${D}/usr" install || die

	# Install documentation
	#cp -R share "$D"/usr/share
	#mkdir -p "$D"/usr/share/man
	#mv "$pkgdir"/usr/share/man3 "$D"/usr/share/man/man3
	dodoc -r share/doc/${PN}/*
	doman share/man3/*

	# Correct pkgconfig file
	sed -e s:PKG_PREFIX:/usr:g \
		< lib/pkgconfig/libspotify.pc \
		> "$D"/usr/lib/pkgconfig/libspotify.pc
}
