# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 toolchain-funcs

DESCRIPTION="suckless linux base utils"
HOMEPAGE="https://core.suckless.org/ubase/"

EGIT_REPO_URI="git://git.suckless.org/ubase"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="static"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	local cflags=""
	local ldflags=""
	use static && {
		cflags+=" -static"
		ldflags+="-static"
	}
	make CC="$(tc-getCC)" CFLAGS="${CFLAGS} ${cflags}" LDFLAGS="${LDFLAGS} ${ldflags}" ubase-box
}

src_install() {
	dodir /usr/sbin/ubase
	make CC="$(tc-getCC)" PREFIX="${D}/usr/sbin/ubase" ubase-box-install
	mv "${D}/usr/sbin/ubase/share/man" "${D}/usr/sbin/ubase/man"
	rm -rf "${D}/usr/sbin/ubase/share"
	mv "${D}/usr/sbin/ubase/bin"/* "${D}/usr/sbin/ubase"
	rm -rf "${D}/usr/sbin/ubase/bin"
	cd "${D}/usr/sbin/ubase/"
	for f in *
	do
		[ -z "$f" ] && continue
		[ "$f" = "man" ] && continue
		[ "$f" = "ubase-box" ] && mv "$f" ..
		rm -f "$f"
		ln -fs ../ubase-box "$f"
	done
}
