# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# extracted from:
# curl -s https://raw.githubusercontent.com/redox-os/ion/master/Cargo.lock | grep -oE '^"checksum\s.*"' | awk '{print $2 "-" $3}' | xclip -selection clipboard -i
# NOTE: Some deps are fetched directly from git so need to be removed from this list
CRATES="
"

inherit cargo git-r3

DESCRIPTION="Cross-platform Rust rewrite of the GNU coreutils."
HOMEPAGE="https://github.com/uutils/coreutils"
SRC_URI="$(cargo_crate_uris ${CRATES})"

EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="fetch-crates test"

DEPEND+="
	>=virtual/rust-1.30.0
"

src_unpack() {
	git-r3_src_unpack
	cd "${WORKDIR}/${P}" || die

	#cargo_src_unpack

	export CARGO_HOME="${ECARGO_HOME}"

	# fetch git-dependencies. network-sandbox isn't active here so we can avoid using RESTRICT="network-sandbox"
	find . -name "Cargo.toml" -exec cargo fetch --manifest-path {} \;
}

src_compile() {
	cargo_src_compile
}

src_install() {
	dobin target/release/${PN}
}

src_test() {
	cargo test
}
