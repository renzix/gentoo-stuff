# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A tool to probe for hardware and upload result to the Linux Hardware Database"
HOMEPAGE="https://linux-hardware.org/"
SRC_URI="https://github.com/linuxhw/hw-probe/releases/download/1.4/${PN}-1.4-129-x86_64.AppImage"

LICENSE="LGPL-2"
SLOT="${PV}"
KEYWORDS="~amd64 ~x86"
IUSE=""

inherit appimage

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
