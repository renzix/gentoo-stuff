# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="scripts for using openrc without sysvinit (shutdown, reboot, poweroff and halt)"
HOMEPAGE="https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/openrc-shutdown-scripts/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~*"
IUSE=""

# To Do: Specify a version greater than or equal to the version of openrc that added support for running with /sbin/openrc-init
DEPEND="sys-apps/openrc"
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_install() {
	into /
	for f in {shutdown,halt,reboot,poweroff}
	do
		dosbin "${FILESDIR}/$f"
	done
}
