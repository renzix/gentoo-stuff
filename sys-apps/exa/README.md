# exa

This contains a live ebuild for exa.

## Recommended patches:

Add these to `/etc/portage/patches/sys-apps/exa`

* **--icons option** - (https://github.com/ogham/exa/pull/368) `wget https://patch-diff.githubusercontent.com/raw/ogham/exa/pull/368.diff`
* **make exa on symlinks to directories show their contents** - (https://github.com/ogham/exa/pull/373) `wget https://patch-diff.githubusercontent.com/raw/ogham/exa/pull/373.diff`
* **fixed git status not working in directories containing '../'** - (https://github.com/ogham/exa/pull/378) `wget https://patch-diff.githubusercontent.com/raw/ogham/exa/pull/378.diff`

