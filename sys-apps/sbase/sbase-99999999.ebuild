# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 toolchain-funcs

DESCRIPTION="suckless unix tools"
HOMEPAGE="https://core.suckless.org/sbase/"

EGIT_REPO_URI="git://git.suckless.org/sbase"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="static"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
	local cflags=""
	use static && cflags=" -static"
	make CC="$(tc-getCC)" CFLAGS="${CFLAGS} ${cflags}" sbase-box
}

src_install() {
	dodir /usr/bin/sbase/man/man1
	dobin sbase-box
	./sbase-box | sed 's|\s|\n|g' | while read -r cmd
	do
		[ -z "$cmd" ] && continue
		ln -fs ../sbase-box "${D}/usr/bin/sbase/$cmd"
	done
	insinto /usr/bin/sbase/man/man1
	for f in *.1
	do
		doins "$f"
	done
}
