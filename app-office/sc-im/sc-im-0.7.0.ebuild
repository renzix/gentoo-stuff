# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A spreadsheet program based on SC"
HOMEPAGE="https://github.com/andmarti1424/sc-im"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
else
	SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
fi

LICENSE="BSD-4"
SLOT="0"
KEYWORDS=""
IUSE="xls"

DEPEND="
	dev-libs/libxml2
	dev-libs/libzip
	xls? ( dev-libs/libxls )
"
RDEPEND="${DEPEND}"
BDEPEND=""

RESTRICT="installsources"

src_compile() {
	cd src
	make prefix="/usr"
}

src_install() {
	cd src
	make prefix="/usr" DESTDIR="${ED}" install
	# Don't conflict with app-i18n/scim
	# not needed for -9999
	[ -f "${ED}/usr/bin/scim" ] && {
		mv "${ED}/usr/bin/scim" "${ED}/usr/bin/sc-im"
	}
}
