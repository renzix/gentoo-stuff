# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit user systemd

DESCRIPTION="API Support for your favorite torrent trackers."
HOMEPAGE="https://github.com/Jackett/Jackett"
SRC_URI="${HOMEPAGE}/releases/download/v${PV}/Jackett.Binaries.Mono.tar.gz -> ${PN}-${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="systemd"

DEPEND=""
RDEPEND="
	${DEPEND}
	dev-lang/mono
	net-misc/curl
	systemd? ( >=sys-apps/systemd-209:= )
"

S="${WORKDIR}/Jackett"

src_install() {
	cd ..
	mkdir -p "${ED}/var/lib"
	mv "Jackett" "${ED}/var/lib"

	newinitd "${FILESDIR}"/jackett.initd.10 jackett

	# I don't use systemd so this is completely untested
	# good luck ;)
	use systemd && {
		systemd_dounit "${FILESDIR}"/jackett.service
		systemd_install_serviced "${FILESDIR}"/jackett.service.conf
	}
}

pkg_postinst() {
	enewgroup jackett
	enewuser jackett -1 -1 /var/lib/Jackett jackett

	chown -R jackett:jackett "${EROOT%/}"/var/lib/Jackett || die
}
