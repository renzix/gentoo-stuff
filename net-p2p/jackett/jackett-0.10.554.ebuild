# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit user systemd

DESCRIPTION="API Support for your favorite torrent trackers."
HOMEPAGE="https://github.com/Jackett/Jackett"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
	KEYWORDS=""
else
	SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${PN}-${PV}.tar.gz"
	KEYWORDS="~amd64"
	S="${WORKDIR}/Jackett-${PV}"
fi

LICENSE="GPL-2"
SLOT="0"

IUSE="debug systemd"

RESTRICT="${RESTRICT} network-sandbox"

DEPEND="
	( >=dev-dotnet/dotnetcore-sdk-bin-2.2.101 <=dev-dotnet/dotnetcore-sdk-bin-3.0.0 )
"
RDEPEND="
	${DEPEND}
	!net-p2p/jackett-bin
	dev-lang/mono
	net-misc/curl
	systemd? ( >=sys-apps/systemd-209:= )
"

src_prepare() {
	patch -p1 -i "${FILESDIR}/fix-jackett-version.diff" || die "Failed to apply fix-jackett-version.diff"
	patch -p1 -i "${FILESDIR}/fix-jackett-app-version.diff" || die "Failed to apply fix-jackett-app-version.diff"
	sed -i "s|{{JACKETT_VERSION}}|${PV}|g" src/Jackett.Common/Utils/EnvironmentUtil.cs
	sed -i "s|{{JACKETT_VERSION}}|${PV}|g" src/Jackett.Common/Models/DTO/ServerConfig.cs

	default
}

src_compile() {
	local runtime=gentoo-x64
	use elibc_musl && runtime=alpine-x64
	local build_type=Debug
	! use debug && build_type=Release
	cd src || die
	addpredict /opt/dotnet_core/sdk/NuGetFallbackFolder
	env \
		DOTNET_SKIP_FIRST_TIME_EXPERIENCE=1 \
		DOTNET_CLI_TELEMETRY_OPTOUT=1 \
			dotnet publish Jackett.Server -f netcoreapp2.2 -r $runtime -c $build_type --verbosity detailed
}

src_install() {
	local runtime=gentoo-x64
	use elibc_musl && runtime=alpine-x64
	local build_type=Debug
	! use debug && build_type=Release

	mkdir -p "${ED}/var/lib"
	mv src/Jackett.Server/bin/$build_type/*/$runtime/publish ./Jackett
	mv "Jackett" "${ED}/var/lib"

	newinitd "${FILESDIR}"/jackett.initd.10 jackett

	# I don't use systemd so this is completely untested
	# good luck ;)
	use systemd && {
		systemd_dounit "${FILESDIR}"/jackett.service
		systemd_install_serviced "${FILESDIR}"/jackett.service.conf
	}
}

pkg_postinst() {
	enewgroup jackett
	enewuser jackett -1 -1 /var/lib/Jackett jackett

	chown -R jackett:jackett "${EROOT%/}"/var/lib/Jackett || die
}
