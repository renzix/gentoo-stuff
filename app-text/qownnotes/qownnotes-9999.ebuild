# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

EGIT_REPO_URI="git://github.com/pbek/QOwnNotes.git"
inherit desktop eutils git-r3 l10n qmake-utils

DESCRIPTION="Open source notepad with Owncloud support"
HOMEPAGE="http://www.qownnotes.org"

PLOCALES="ar bn ca ceb cs de el_GR en es fil fr hil hi hr hu id it ja nl pcm_NG pl pt_BR pt_PT ru sv tl tr uk ur zh_CN zh_TW"
IUSE=""

for my_locale in ${PLOCALES}; do
	IUSE+=" l10n_${my_locale}"
done

SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5
	dev-qt/qtsql:5
	dev-qt/qtsvg:5
	dev-qt/qtwidgets:5
"
DEPEND="${RDEPEND}"

DOCS=( CHANGELOG.md README.md SHORTCUTS.md )

src_prepare() {
	l10n_find_plocales_changes "${S}/src/languages" "QOwnNotes_" '.ts'
}

src_configure() {
	eqmake5 src/QOwnNotes.pro
}

src_install() {
	dobin QOwnNotes

	for size in {16x16,24x24,32x32,48x48,64x64,96x96,128x128,256x256,512x512}; do
		doicon -s $size src/images/icons/$size/apps/QOwnNotes.png
	done

	doicon -s scalable src/images/icons/scalable/apps/QOwnNotes.svg

	domenu src/PBE.QOwnNotes.desktop

	install -d "${ED}/usr/share/QOwnNotes/languages"

	for my_locale in ${PLOCALES[@]}
	do
		use l10n_${my_locale} && install -D -m644 src/languages/QOwnNotes_${my_locale}.qm "${ED}/usr/share/QOwnNotes/languages/"
	done

	einstalldocs
}
