# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/BoostIO/${PN}.git"
	S="${WORKDIR}/boostnote-${PV}"
	KEYWORDS=""
else
	SRC_URI="https://github.com/BoostIO/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	RESTRICT="primaryuri"
	S="${WORKDIR}/Boostnote-${PV}"
	KEYWORDS="~amd64 ~x86"
fi

inherit desktop

DESCRIPTION="Open source note-taking app for programmers"
HOMEPAGE="https://github.com/BoostIO/boostnote"

LICENSE="GPL-3"
SLOT="0"
IUSE="vanilla"

DEPEND="
	net-libs/nodejs[npm]
"
RDEPEND="
	>=net-libs/nodejs-7.10.1
	>=virtual/electron-3.0.8:3.0
"

src_unpack() {
	default
	type git-r3_src_unpack > /dev/null 2>&1 && git-r3_src_unpack

	cd "${S}" || die
	npm install --verbose --production --no-optional --no-shrinkwrap || die
	mv node_modules "${T}"/node_modules-production || die
	npm install --verbose --no-optional --no-shrinkwrap || die
	npm install --verbose grunt-cli || die
	mv node_modules "${T}"/node_modules-dev || die
}

src_prepare() {
	#! use vanilla && patch -Np1 -i "${FILESDIR}/remove-analytics.patch" || die "failed to apply patch remove-analytics.patch"

	default
}

src_compile() {
	mv "${T}/node_modules-dev" node_modules || die
	./node_modules/.bin/grunt compile || die
	rm -r node_modules || die
	mv "${T}/node_modules-production" node_modules || die
}

src_install() {
	#einstalldocs

	#dodoc "${D}/usr/$(get_libdir)/node_modules/${PN}/LICENSE"

	cp "${FILESDIR}/${PN}.js" "${T}"
	sed -i 's|{{ELECTRON_VERSION}}|electron-3.0|g' "${T}/${PN}.js"

	exeinto /usr/bin
	newexe "${T}/${PN}.js" "${PN}"

	# Remove stuff we do not need
	! use vanilla && find node_modules \
		-name "*.a" -exec rm '{}' \; \
		-or -name "*.bat" -exec rm '{}' \; \
		-or -name "*.node" -exec chmod a-x '{}' \; \
		-or -name "benchmark" -prune -exec rm -r '{}' \; \
		-or -name "doc" -prune -exec rm -r '{}' \; \
		-or -name "html" -prune -exec rm -r '{}' \; \
		-or -name "man" -prune -exec rm -r '{}' \; \
		-or -path "*/less/gradle" -prune -exec rm -r '{}' \; \
		-or -path "*/task-lists/src" -prune -exec rm -r '{}' \;

	dodir /usr/lib/${PN}
	cp -a * "${D}usr/lib/${PN}"

	newicon resources/app.png "${PN}.png"
	domenu "${FILESDIR}/${PN}.desktop"
}
