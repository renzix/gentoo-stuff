# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Tiling script for kwin"
HOMEPAGE="https://github.com/faho/kwin-tiling"

inherit git-r3

EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

DEPEND="kde-frameworks/plasma:="
RDEPEND="
	${DEPEND}
	kde-plasma/kwin:=
"
BDEPEND=""

src_compile() {
	mkdir -p "${T}/pkg" || die "Failed to create directory ${T}/pkg"
	HOME="${T}/pkg" plasmapkg2 --type kwinscript -i . || die
}

src_install() {
	install -d "${D}/usr/share/kwin/scripts/kwin-script-tiling"
	cp -ra "${T}/pkg/.local/share/kwin/scripts/kwin-script-tiling/." "${D}/usr/share/kwin/scripts/kwin-script-tiling/"
	install -Dm644 ./metadata.desktop "${D}/usr/share/kservices5/kwin-script-tiling.desktop"
}
