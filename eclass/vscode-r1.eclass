# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: vscode.eclass
# @MAINTAINER:
# ahrs
# @AUTHOR:
# ahrs
# @BLURB: 
# @DESCRIPTION: This is a small eclass that sets the required version of nodejs to be used by code and extensions

DEPEND="
unsupported-node? ( >=net-libs/nodejs-8.0.0[npm] )
!unsupported-node? ( >=net-libs/nodejs-8.0.0[npm] <=net-libs/nodejs-11.0.0[npm] )
"
