####################################################################
#
# When you add an entry to the top of this file, add your name, the date, and
# an explanation of why something is getting masked. Please be extremely
# careful not to commit atoms that are not valid, as it can cause large-scale
# breakage, especially if it ends up in the daily snapshot.
#
## Example:
##
## # Dev E. Loper <developer@gentoo.org> (28 Jun 2012)
## # Masking  these versions until we can get the
## # v4l stuff to work properly again
## =media-video/mplayer-0.90_pre5
## =media-video/mplayer-0.90_pre5-r1
#
# - Best last rites (removal) practices -
# Include the following info:
# a) reason for masking
# b) bug # for the removal (and yes you should have one)
# c) date of removal (either the date or "in x days")
#
## Example:
##
## # Dev E. Loper <developer@gentoo.org> (23 May 2015)
## # Masked for removal in 30 days.  Doesn't work
## # with new libfoo. Upstream dead, gtk-1, smells
## # funny. (bug #987654)
## app-misc/some-package

#--- END OF EXAMPLES ---

# ahrs <mail@ahrs.me> 26 Nov 2018)
# beta
=app-shells/bash-5.0_beta2

# ahrs <mail@ahrs.me> 17 Nov 2018)
# development version
=www-client/chromium-72.0.3602.2
=www-client/chromium-72.0.3608.4
=www-client/chromium-72.0.3610.2

# ahrs <mail@ahrs.me 16 Nov 2018)
# development version
=app-editors/vscode-99999999

# ahrs <mail@ahrs.me 16 Nov 2018)
# development version
=net-libs/nodejs-99999999

# ahrs <mail@ahrs.me> (01 Nov 2018)
# development release
>=media-sound/picard-2.0.5

# ahrs <mail@ahrs.me> (01 Nov 2018)
# development release
>=net-libs/nodejs-9999999

# ahrs <mail@ahrs.me> (28 Sept 2018)
# development release
=app-misc/dvtm-9999

# ahrs <mail@ahrs.me> (09 Sept 2018)
# Development release for testing purposes only
# (may be broken)
=media-video/freetube-9999

# ahrs <mail@ahrs.me> (07 Sept 2018)
# Development release for testing purposes only
# (may be broken)
=app-text/boostnote-9999

# ahrs <mail@ahrs.me> (31 Aug 2018)
# Pre-release version for testing purposes only
net-p2p/jackett-bin:pre
net-p2p/jackett-bin:0/pre

# ahrs <mail@ahrs.me> (30 Aug 2018)
# Development version only in the overlay for testing purposes.
# This could break at any moment.
=net-dns/pi-hole-ftl-9999
