# fakeverlay

This is a fake overlay to be used for CI purposes. Some dependencies theoretically exist but just aren't in the Gentoo tree yet (for example dev-util/electron-bin newer than dev-util/electron). To prevent `repoman` from throwing error messages about dependency.badindev a fake overlay is added. Ideally the maintainers of these packages would update them in the Gentoo tree so that you don't have to grab these packages from some other overlay.

## packages

* dev-util/electron - Only 1.7 is in the Gentoo tree. You can grab binaries for >=2.0 from the [chaoslab overlay](https://gitlab.com/chaoslab/chaoslab-overlay)
