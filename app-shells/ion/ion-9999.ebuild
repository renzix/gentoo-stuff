# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# extracted from:
# curl -s https://raw.githubusercontent.com/redox-os/ion/master/Cargo.lock | grep -oE '^"checksum\s.*"' | awk '{print $2 "-" $3}' | xclip -selection clipboard -i
# NOTE: Some deps are fetched directly from git so need to be removed from this list
CRATES="
aho-corasick-0.6.9
ansi_term-0.11.0
atty-0.2.11
backtrace-0.3.9
backtrace-sys-0.1.24
bitflags-1.0.4
bytecount-0.3.2
byteorder-1.2.7
calculate-0.5.1
cc-1.0.25
cfg-if-0.1.6
clap-2.32.0
cloudabi-0.0.3
decimal-2.0.4
failure-0.1.3
failure_derive-0.1.3
fuchsia-zircon-0.3.3
fuchsia-zircon-sys-0.3.3
glob-0.2.11
hashbrown-0.1.2
itoa-0.4.3
lazy_static-1.1.0
libc-0.2.43
liner-0.4.5
memchr-2.1.1
numtoa-0.1.0
ord_subset-3.1.1
permutate-0.3.2
proc-macro2-0.4.20
quote-0.6.9
rand-0.5.5
rand_core-0.2.2
rand_core-0.3.0
redox_syscall-0.1.40
redox_termios-0.1.1
regex-1.0.5
regex-syntax-0.6.2
rustc-demangle-0.1.9
rustc-serialize-0.3.24
scopeguard-0.3.3
serde-1.0.80
small-0.1.0
smallvec-0.6.5
strsim-0.7.0
syn-0.15.17
synstructure-0.10.1
termion-1.5.1
textwrap-0.10.0
thread_local-0.3.6
ucd-util-0.1.2
unicode-segmentation-1.2.1
unicode-width-0.1.5
unicode-xid-0.1.0
unreachable-1.0.0
users-0.7.0
utf8-ranges-1.0.2
vec_map-0.8.1
version_check-0.1.5
void-1.0.2
winapi-0.3.6
winapi-i686-pc-windows-gnu-0.4.0
winapi-x86_64-pc-windows-gnu-0.4.0
xdg-2.1.0
"

CRATES=""

inherit cargo git-r3

DESCRIPTION="The Ion Shell. Compatible with Redox and Linux."
HOMEPAGE="https://gitlab.redox-os.org/redox-os/ion"
SRC_URI="$(cargo_crate_uris ${CRATES})"

EGIT_REPO_URI="
	https://gitlab.redox-os.org/redox-os/${PN}.git
	https://github.com/redox-os/${PN}.git
"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="+examples fetch-crates +man test"

DEPEND+="
	>=virtual/rust-1.30.0
	man? ( app-text/mdbook )
"

src_unpack() {
	git-r3_src_unpack
	cd "${WORKDIR}/${P}" || die

	#cargo_src_unpack

	export CARGO_HOME="${ECARGO_HOME}"

	# fetch git-dependencies. network-sandbox isn't active here so we can avoid using RESTRICT="network-sandbox"
	find . -name "Cargo.toml" -exec cargo fetch --manifest-path {} \;
}

src_compile() {
	cargo_src_compile
	use examples && {
		cd manual || die
		mdbook build || die
	}
}

src_install() {
	# don't compress examples
	docompress -x /usr/share/doc/${PF}/examples

	# install the shell into /bin instead of /usr/bin
	# this is consistent with the other shells in the app-shells category
	into /
	dobin target/release/${PN}
	use examples && dodoc -r examples
	use man && dodoc -r manual/book
}

src_test() {
	emake tests
}

pkg_postinst() {
	ebegin "Updating /etc/shells"
	( grep -v "^/bin/ion$" /etc/shells; echo "/bin/ion" ) > "${T}"/shells
	mv -f "${T}"/shells /etc/shells
	eend $?
}
