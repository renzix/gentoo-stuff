# bash

This is a carbon-copy of the ebuilds from the Gentoo tree with the option to compile it statically. The necessary changes for this are included in the Gentoo tree but commented out. The only thing it's missing is an `IUSE="static"`.
