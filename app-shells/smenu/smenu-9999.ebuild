# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools git-r3

DESCRIPTION="Terminal utility to use standard input to create a nice selection window"
HOMEPAGE="https://github.com/p-gen/smenu"

EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

# Fix Me: https://github.com/p-gen/smenu/tree/master/tests
RESTRICT="test"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	eautoreconf || die
	default
}

src_configure() {
	./configure --prefix=/usr || die
}
