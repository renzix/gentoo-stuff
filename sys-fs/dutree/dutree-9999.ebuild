# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

#CRATES="
#"

inherit bash-completion-r1 cargo git-r3

DESCRIPTION="a tool to analyze file system usage written in Rust"
HOMEPAGE="https://github.com/nachoparker/dutree"
#SRC_URI="$(cargo_crate_uris ${CRATES})"
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""

IUSE="fetch-crates"

RESTRICT="mirror network-sandbox"
DEPEND=">=virtual/rust-1.17"
RDEPEND="${DEPEND}"

src_install() {
	cargo_src_install
	#newbashcomp contrib/completions.bash exa
	#insinto /usr/share/zsh/site-functions
	#newins contrib/completions.zsh _exa
	#insinto /usr/share/fish/vendor_completions.d
	#newins contrib/completions.fish exa.fish
	#doman contrib/man/*
}
