# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A FUSE filesystem intended to be used like Apache mod_rewrite"
HOMEPAGE="https://github.com/sloonz/rewritefs"
EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-libs/libpcre
	sys-fs/fuse:0"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

inherit git-r3

src_prepare() {
	sed -i "s|-O2|$CFLAGS|g" Makefile
	sed -i "s|PREFIX = /usr/local|PREFIX = /usr|g" Makefile

	default
}
