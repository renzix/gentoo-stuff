# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit cmake-utils

DESCRIPTION="Yet another unofficial speedtest.net client cli interface"
HOMEPAGE="https://github.com/taganaka/SpeedTest"

if [[ -z ${PV%%*9999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
else
	SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	S="${WORKDIR}/SpeedTest-${PV}"
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="ssl libressl"

REQUIRED_USE="ssl"

RDEPEND="
	net-misc/curl:=
	!libressl? (
		dev-libs/openssl:=
	)
	libressl? (
		dev-libs/libressl:=
	)
	dev-libs/libxml2:=
"
DEPEND="
dev-util/cmake:=
${RDEPEND}
"
BDEPEND=""
