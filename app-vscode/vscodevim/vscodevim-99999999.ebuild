# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit vscode-r1

DESCRIPTION="Vim for Visual Studio Code"
HOMEPAGE="https://github.com/VSCodeVim/Vim"

RESTRICT="network-sandbox"

if [[ -z ${PV%%*99999999} ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"
	KEYWORDS=""
else
	SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="-* ~amd64 ~x86"
fi

LICENSE="MIT"
SLOT="0"

IUSE="unsupported-node"

DEPEND="${DEPEND}"
RDEPEND="
	${DEPEND}
	unsupported-node? ( app-editors/vscode[unsupported-node] )
	!unsupported-node? ( app-editors/vscode )
"
BDEPEND=""

src_unpack() {
	default
	type git-r3_src_unpack > /dev/null 2>&1 && git-r3_src_unpack

	cd "${S}" || die
}

src_compile() {
	npm --verbose install || die
	export NODE_ENV=production
	npm --verbose run-script build || die
}

src_install() {
	dodir /usr/share/code-oss/extensions || die
	mv "${S}" "${D}/usr/share/code-oss/extensions/${P}" || die
}

pkg_postinst() {
	elog "Symlink /usr/share/code-oss/extensions/${P} to \$HOME/.vscode-oss/extensions/${P}"
	elog "# ln -s /usr/share/code-oss/extensions/${P} \$HOME/.vscode-oss/extensions/${P}"
}

pkg_postrm() {
	elog "Don't forget to remove any dangling symlinks you've created in \$HOME/.vscode-oss/extensions"
}