# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A lightweight Wayland notification daemon"
HOMEPAGE="https://wayland.emersion.fr/mako"

inherit git-r3 meson

EGIT_REPO_URI="https://github.com/emersion/mako.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="elogind systemd"

REQUIRED_USE="
	!systemd? ( elogind )
	!elogind? ( systemd )
"

DEPEND="
	>=dev-libs/wayland-1.16.0
	>=dev-libs/wayland-protocols-1.16
	>=x11-libs/cairo-1.16.0-r2
	>=x11-libs/pango-1.42.4
	systemd? ( sys-apps/systemd )
	elogind? ( sys-auth/elogind )
"
RDEPEND="
	${DEPEND}
	sys-apps/dbus
"
BDEPEND=""

src_install() {
	dodoc "README.md"

	meson_src_install
}
