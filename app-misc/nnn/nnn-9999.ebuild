# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit bash-completion-r1 git-r3 toolchain-funcs

DESCRIPTION="The missing terminal file browser for X"
HOMEPAGE="https://github.com/jarun/nnn"
EGIT_REPO_URI="${HOMEPAGE}.git"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""

IUSE="+bash-completion fish-completion zsh-completion"

DEPEND="sys-libs/ncurses:0=
	sys-libs/readline:0="
RDEPEND="
	${DEPEND}
	fish-completion? ( app-shells/fish )
	zsh-completion? ( app-shells/zsh )
"

src_prepare() {
	default
	tc-export CC
	sed -i -e '/strip/d' Makefile || die "sed failed"

}

src_install() {
	emake PREFIX="/usr" DESTDIR="${D}" install

	use bash-completion && newbashcomp scripts/auto-completion/bash/nnn-completion.bash nnn

	use fish-completion && {
		insinto /usr/share/fish/vendor_completions.d
		doins scripts/auto-completion/fish/nnn.fish
	}

	use zsh-completion && {
		insinto /usr/share/zsh/site-functions
		doins scripts/auto-completion/zsh/_nnn
	}

	einstalldocs
}
