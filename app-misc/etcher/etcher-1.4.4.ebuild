# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Flash OS images to SD cards & USB drives, safely and easily."
HOMEPAGE="https://github.com/resin-io/etcher"

SRC_URI="
	"https://github.com/resin-io/etcher/releases/download/v${PV}/etcher-electron-${PV}-x86_64.AppImage"
"

inherit appimage

LICENSE="Apache-2.0"
SLOT="${PV}"
KEYWORDS="~amd64"

#S="${DISTDIR}"
