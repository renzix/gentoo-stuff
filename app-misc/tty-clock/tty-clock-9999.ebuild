# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

DESCRIPTION="tty-clock"
HOMEPAGE="https://github.com/xorg62/tty-clock"
EGIT_REPO_URI="https://github.com/xorg62/tty-clock"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND=""
DEPEND="${RDEPEND}"

src_prepare() {
	sed -i "s|puts(\"TTY-Clock 2 © devel version\");|puts(\"TTY-Clock 2 © devel #$(git rev-parse --short HEAD)\");|g" ttyclock.c

	default
}

src_install() {
	dobin tty-clock
	doman "${PN}.1"

	dodoc README
}
