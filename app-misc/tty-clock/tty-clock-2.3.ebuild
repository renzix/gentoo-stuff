# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="tty-clock"
HOMEPAGE="https://github.com/xorg62/tty-clock"
SRC_URI="${HOMEPAGE}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~*"

RDEPEND=""
DEPEND="${RDEPEND}"

src_prepare() {
	sed -i "s|puts(\"TTY-Clock 2 © devel version\");|puts(\"TTY-Clock 2 © version ${PV}\");|g" ttyclock.c

	default
}

src_install() {
	dobin tty-clock
	doman "${PN}.1"

	dodoc README
}
