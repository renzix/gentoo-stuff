# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python3_{4,5,6} pypy{,3} )

inherit distutils-r1 git-r3

DESCRIPTION="DuckDuckGo from the terminal"
HOMEPAGE="https://github.com/jarun/ddgr"
#SRC_URI="https://github.com/jarun/ddgr/archive/v1.2.tar.gz"
EGIT_REPO_URI="https://github.com/jarun/ddgr"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-python/requests
"
DEPEND="
	${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
"

#S=${WORKDIR}/ddgr-1.2

src_compile() {
	emake
}

src_install() {
	emake PREFIX="/usr" DESTDIR="${D}" install

	dodoc README.md CHANGELOG
}
